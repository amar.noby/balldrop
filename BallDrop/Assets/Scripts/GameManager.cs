using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Transform ball;
    public GameObject obstacle1;
    public GameObject obstacle2;
    GameObject[] obstacles = new GameObject[10];
    public float offset = -2;

    public GameObject[,] Obstacles = new GameObject[2,5];
    // Start is called before the first frame update
    void Start()
    {
        CreateObstaclePool();
        InvokeRepeating("PlaceObstacles", 3, 2);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //void CreateObstaclePool()
    //{
    //    for(int i = 0; i < obstacles.Length / 2; i++)
    //    {
    //        obstacles[i] = Instantiate(obstacle1);
    //    }
    //    for (int i = obstacles.Length / 2; i < obstacles.Length; i++)
    //    {
    //        obstacles[i] = Instantiate(obstacle2);
    //    }
    //}

    //void PlaceObstacles()
    //{
    //    int randomObstacle = Random.Range(0, obstacles.Length);
    //    float randomPosition = Random.Range(1f, 2f);
    //    if (ball)
    //    {
    //        if (randomObstacle >= 5)
    //        {
    //            obstacles[randomObstacle].transform.position = new Vector2(randomPosition, ball.transform.position.y + offset);
    //        }
    //        else
    //        {
    //            obstacles[randomObstacle].transform.position = new Vector2(-randomPosition, ball.transform.position.y + offset);
    //        }
    //    }
    //}

    void CreateObstaclePool()
    {
        for (int i = 0; i < 5; i++)
        {
            Obstacles[0,i] = Instantiate(obstacle1);
            Obstacles[0, i].SetActive(false);
        }
        for (int i = 0; i < 5; i++)
        {
            Obstacles[1, i] = Instantiate(obstacle2);
            Obstacles[1, i].SetActive(false);
        }
    }

    void PlaceObstacles()
    {
        int randomObstacle = Random.Range(0,2);
        float randomPosition = Random.Range(1f, 2f);
        
        if (ball)
        {
            if (randomObstacle == 0)
            {
                for(int i = 0; i < 5; i++)
                {
                    if (!Obstacles[randomObstacle, i].activeInHierarchy)
                    {
                        Obstacles[randomObstacle,i].SetActive(true);
                        Obstacles[randomObstacle,i].transform.position = new Vector2(-randomPosition, ball.transform.position.y + offset);
                        break;
                    }
                }
            }
            else
            {
                for (int i = 0; i < 5; i++)
                {
                    if (!Obstacles[randomObstacle, i].activeInHierarchy)
                    {
                        Obstacles[randomObstacle, i].SetActive(true);
                        Obstacles[randomObstacle, i].transform.position = new Vector2(randomPosition, ball.transform.position.y + offset);
                        break;
                    }
                }
            }
        }
    }
}
