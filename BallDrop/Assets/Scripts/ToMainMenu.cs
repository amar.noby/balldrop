using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ToMainMenu : MonoBehaviour
{
    public Leaderboard leaderboard;
   public void MainMenu()
    {
        leaderboard.SaveLeaderboard();
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

    public void Reset()
    {
        StartCoroutine(ResetScene());
    }

    IEnumerator ResetScene()
    {
        Score.score = 0;
        leaderboard.SaveLeaderboard();
        yield return new WaitForSeconds(0.5f);
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
