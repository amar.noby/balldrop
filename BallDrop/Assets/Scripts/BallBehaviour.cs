using Mono.Cecil.Cil;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class BallBehaviour : MonoBehaviour
{
    Rigidbody2D rb;
    public float speed;
    bool rightIsPressed;
    bool leftIsPressed;
    public float driftSpeed;
    public float torqueSpeed;
    Animator animator;

    public GameObject DimPanel;
    public GameObject LeaderboardPanel;

    bool moving = true;

    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(moving)
        {
            rb.velocity = Vector2.down * Time.timeSinceLevelLoad * speed * Time.deltaTime;
        }
        else
        {
            rb.velocity = Vector2.zero;
        }
        if (rightIsPressed)
        {
            rb.AddForce(Vector2.right * driftSpeed);
            rb.AddTorque(-torqueSpeed);
        }
        if (leftIsPressed)
        {
            rb.AddForce(Vector2.left * driftSpeed);
            rb.AddTorque(torqueSpeed);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            StartCoroutine(DestroyBall());
        }
    }
    IEnumerator DestroyBall()
    {
        moving = false;
        audioSource.Play();
        animator.SetTrigger("boom");
        yield return new WaitForSeconds(0.25f);
        Destroy(gameObject);
        DimPanel.SetActive(true);
        LeaderboardPanel.SetActive(true);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            StartCoroutine(DeactivateObstacle(collision));
            Score.score += 1;
        }
    }
    IEnumerator DeactivateObstacle(Collider2D collision)
    {
        yield return new WaitForSeconds(2);
        collision.gameObject.SetActive(false);
    }
    public void PushBallRight(bool buttonState)
    {
        rightIsPressed = buttonState;
    }
    public void PushBallLeft(bool buttonState)
    {
        leftIsPressed = buttonState;
    }
}
