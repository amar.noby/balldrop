using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
public class MuteButtons : MonoBehaviour
{
    public AudioMixer audioMixer;
    float musicLevel;
    float sfxLevel;
    public void MuteMusic()
    {
        audioMixer.GetFloat("musicVol", out musicLevel);
        if (musicLevel == 0)
        {
            audioMixer.SetFloat("musicVol", -80.0f);
        }
        else
        {
            audioMixer.SetFloat("musicVol", 0);
        }
    }
    public void MuteSFX()
    {
        audioMixer.GetFloat("sfxVol", out sfxLevel);
        if (sfxLevel == 0)
        {
            audioMixer.SetFloat("sfxVol", -80.0f);
        }
        else
        {
            audioMixer.SetFloat("sfxVol", 0);
        }
    }
}
