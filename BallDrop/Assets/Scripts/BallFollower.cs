using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallFollower : MonoBehaviour
{
    public GameObject ball;
    float offset;
    // Start is called before the first frame update
    void Start()
    {
        offset = ball.transform.position.y - transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (ball)
        {
            transform.position = new Vector3 (transform.position.x,ball.transform.position.y - offset,transform.position.z);
        }
    }
}
