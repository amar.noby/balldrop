using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMover : MonoBehaviour
{
    float environmentHight;
    float environmentPushDistance;
    const float pixelsPerUnit = 100;
    public Sprite skySprite;
    // Start is called before the first frame update
    void Start()
    {
        environmentHight = skySprite.rect.size.y / pixelsPerUnit;
        environmentPushDistance = -environmentHight * 2;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            transform.Translate(new Vector2(0, environmentPushDistance));
        }
    }
}
