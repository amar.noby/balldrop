using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour
{
    public void SwitchScene()
    {
        StartCoroutine(MainGame());
    }
    IEnumerator MainGame()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("MainGame");
    }
}
